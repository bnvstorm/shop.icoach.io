---
title: Пакет «Оптимальный»
what: Как пакет «Старт», только перед первым шагом мы сделаем предзапуск, то есть прогреем аудиторию с помощью лидмагнита
why: Лидмагнит познакомит клиентов с вами, докажет им вашу экспертность, и вызовет доверие к вам ещё до консультации или вебинара
contents:
  - Разработка концепции продвижения
  - Лендинги первого шага и основного продукта
  - Подключение CRM и уведомлений о новых заявках
  - Создание контент-плана для соцсетей
  - Первичное наполнение фейсбука и инстаграма
  - Создание лидмагнита
  - Тестовая рекламная кампания в фейсбуке и инстаграме
  - Настройка ретаргетинга на лендинг основного продукта
prices:
  - title: Под ключ $10000
    downpayment: 500
    payref: https://secure.wayforpay.com/button/b11f86ee60358
  - title: Партнёрство $2490 + 30% с будущих продаж
    downpayment: 290
    payref: https://secure.wayforpay.com/button/b6d76c1440354
ref: optimal
permalink: /optimal/
layout: offer
offer-download: /pdf/icoach-offers.pdf
---

