---
title: Продающий лендинг
what: Разработка продающего лендинга для вашего продукта
why: Качественный лендинг просто, понятно, и выгодно объясняет посетителю, почему ему стоит работать именно с вами, отделяет целевых от нецелевых, и побуждает оставить заявку на ваши услуги
contents:
  - Интервью с маркетологом для выявления ключевых смыслов о вас и вашем продукте для лендинга
  - Разработка продающего текста
  - Вёрстка лендинга в стандартный шаблон
prices:
  - price: 450
    payref: https://secure.wayforpay.com/button/b3491c035dd9d
ref: landing-page
permalink: /landing-page/
layout: offer
---

